﻿namespace OtusSQLite
{

    using System.Collections.Generic;
    using Microsoft.EntityFrameworkCore;


    public class AvitoContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<SubCategory> SubCategories { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=Avito.db");
    }

}