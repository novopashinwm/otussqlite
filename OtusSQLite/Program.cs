﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OtusSQLite
{
    class Program
    {
        static readonly Random  rnd = new Random();
        static void Main()
        {
            using (AvitoContext ctx = new AvitoContext()) 
            {    
                //FillingDates(ctx);
                Console.WriteLine($"Максимальная цена: {ctx.Products.Max(p => p.Price)}");
                Console.WriteLine($"Минимальная цена : {ctx.Products.Min(p => p.Price)}");
                Console.WriteLine($"Максимальная дата: {ctx.Products.Max(p => p.DateCreate)}");
                Console.WriteLine($"Минимальная дата: {ctx.Products.Min(p => p.DateCreate)}");
                Console.ReadKey();
            }
        }

        static void FillingDates(AvitoContext ctx)
        {

            #region Заполнение таблица категорий
            string[] catergorArr = new string[] { "Транспорт" , "Недвижимость", "Работа" ,
                "Личные вещи" , "Для дома и дачи" ,  "Для бизнеса", "Бытовая электроника" ,
                "Хобби и отдых", "Животные", "Услуги" };
            
            foreach (var item in catergorArr)
            {
                ctx.Categories.Add(new Category() { Name = item });
            }
            ctx.SaveChanges();
            #endregion

            #region Заполнение таблицы подкатегорий
            string[][] subcatArr = new string[][]
            {
                new string[] {"Транспорт", "Автомобили", "Мотоциклы и мототехника",
                    "Грузовики и спецтехника", "Водный транспорт", "Запчасти и аксессуары" } ,
                new string[] {"Недвижимость", "Квартиры в новостройках", "Квартиры в аренду",
                    "Квартиры посуточно","Дома, дачи, коттеджи","Комнаты", "Коммерческая недвижимость"  },
                new string[] {"Работа", "Вакансии", "Отрасли", "Резюме" },
                new string[] {"Личные вещи", "Одежда, обувь, аксессуары", "Детская одежда и обувь",
                    "Товары для детей и игрушки", "Часы и украшения", "Красота и здоровье" } ,
                new string[] {"Для дома и дачи", "Бытовая техника", "Мебель и интерьер",
                        "Посуда и товары для кухни", "Продукты питания", "Ремонт и строительство", "Растения" } ,
                new string[] {"Для бизнеса"  , "Готовый бизнес",  "Оборудование для бизнеса" },
                new string[] {"Бытовая электроника", "Аудио и видео", "Игры, приставки и программы",
                        "Настольные компьютеры", "Ноутбуки", "Оргтехника и расходники",
                        "Планшеты и электронные книги", "Телефоны", "Товары для компьютера", "Фототехника" } ,
                new string[] {"Хобби и отдых" , "Билеты и путешествия", "Велосипеды","Книги и журналы",
                        "Коллекционирование", "Музыкальные инструменты", "Охота и рыбалка", "Спорт и отдых" },
                new string[] {"Животные","Собаки","Кошки","Птицы","Аквариум","Другие животные","Товары для животных" },
                new string[] {"Услуги","IT, интернет, телеком","Бытовые услуги","Деловые услуги","Искусство","Красота, здоровье",
                        "Доставка, курьеры","Мастер на час","Няни, сиделки","Оборудование, производство","Обучение, курсы",
                        "Охрана, безопасность","Доставка еды и продуктов" }
            };
            List<SubCategory> listSubCat = ctx.SubCategories.ToList();
            for (int i = 0; i < subcatArr.Length; i++)
            {
                Category category = ctx.Categories.Where(x => x.Name == subcatArr[i][0]).First();

                for (int j = 1; j < subcatArr[i].Length; j++)
                {
                    string item = subcatArr[i][j];
                    if (listSubCat.Where(a => a.Name == item && a.Category == category).Count() == 0)
                        ctx.SubCategories.Add(new SubCategory() { Name = item, Category = category });
                }
            }
            listSubCat = ctx.SubCategories.ToList();
            #endregion

            #region Добавление комментов
            string[] comments = new string[] { "Comment01", "Comment02", "Comment03", "Comment04","Comment05"
                , "Comment06", "Comment07", "Comment08", "Comment09","Comment10"};
            List<Comment> listComm = ctx.Comments.ToList();
            foreach (var item in comments)
            {
                if (listComm.Count(cm => cm.Text == item) == 0)
                {
                    ctx.Comments.Add(new Comment() { Text = item });
                }
            }
            ctx.SaveChanges();
            Comment[] arrComm = ctx.Comments.ToArray();
            #endregion

            #region Генерирование товаров для продажи

            foreach (var item in listSubCat)
            {
                List<Comment> comm = new List<Comment>();

                for (int i = 0; i < rnd.Next(1, arrComm.Length); i++)
                    comm.Add(arrComm[rnd.Next(0, arrComm.Length)]);

                var sellers = new string[] {"Анастасия", "Мария", "Дарья", "Анна",
                    "Елизавета" , "Полина", "Виктория", "Екатерина", "Александр",
                    "Максим", "Иван", "Артем", "Дмитрий", "Никита", "Михаил", "Даниил" };
                Product product = new Product()
                {
                    Comments = comm
                    ,
                    DateCreate = DateTime.Now.AddDays(rnd.Next(-100, 0))
                    ,
                    Description = $"Описание товара : {item.GetHashCode()}"
                    ,
                    Seller = sellers[rnd.Next(0, sellers.Length)]
                    ,
                    Phone = $"+79{rnd.Next(100, 999)}{rnd.Next(1_000_000, 10_000_000)}"
                    ,
                    SubCategory = item
                    ,
                    Price = rnd.Next(1000, 800000)
                    ,
                    Name = GetNameProduct()
                };
                ctx.Products.Add(product);
            }
            ctx.SaveChanges();
            #endregion

        }

        private static string GetNameProduct()
        {
            int len = rnd.Next(3, 15);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < len; i++)
            {
                sb.Append((char)('A' + rnd.Next(0, 26)));
            }
            return sb.ToString();
        }
    }
}
