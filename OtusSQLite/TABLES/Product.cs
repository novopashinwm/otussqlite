using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


    public class Product
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Phone { get; set; }
        public string  Seller { get; set; }
        public int Price { get; set; }
        public DateTime DateCreate { get; set; }
        public SubCategory SubCategory { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
    }
