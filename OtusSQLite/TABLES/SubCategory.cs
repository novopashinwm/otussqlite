using System.ComponentModel.DataAnnotations;


    public class SubCategory
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public Category Category { get; set; }
    }
