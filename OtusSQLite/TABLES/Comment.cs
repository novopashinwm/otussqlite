using System.ComponentModel.DataAnnotations;


    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public string Text { get; set; }
    }
